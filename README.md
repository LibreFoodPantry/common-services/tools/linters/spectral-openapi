# Spectral

A linter for OpenAPI files.
[Spectral](https://stoplight.io/open-source/spectral).

## Using in the Pipeline

To enable spectral in your project's pipeline add
`spectral` to the `ENABLE_JOBS`
variable in your project's `.gitlab-ci.yaml` e.g.:

```bash
variables:
  ENABLE_JOBS: "spectral"
```

## Using in Visual Studio Code

Install the [Spectral extension](https://marketplace.visualstudio.com/items?itemName=stoplight.spectral)
for Visual Studio Code.

## Using Locally in Your Development Environment

Spectral can be run locally in a linux-based bash shell or in a linting shell
script with this Docker command:

```bash
docker run --rm -v "${PWD}":/app/project -w /app/project
    --entrypoint "" stoplight/spectral
    spectral lint -F warn ./**/*openapi*.{yml,yaml}

```

## Configuration

Projects using Spectral must have a `.spectral.yaml` file (at the top level
of the project) specifying the
[ruleset](https://meta.stoplight.io/docs/spectral/9ffa04e052cc1-spectral-cli#using-a-ruleset-file)
to be used when validating.

Here is an example of a minimal `.spectral.yaml` file:

```yaml
---
extends: ["spectral:oas"]
```

The Spectral tool as configured in the pipeline and in the local Docker
command above will lint all files with `openapi` somewhere in the filename,
and with an extension of `.yml` or `.yaml`. (I.e. the Unix glob
used to find files is `**/*openapi*.{yml,yaml}`)

The pipeline will fail for all warnings and errors.

## Licensing

* Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
* Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
* Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
